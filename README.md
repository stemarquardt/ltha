# Logikcull Take Home Assignment
 - [App architecture](#architecture)
 - [Next steps](#next-steps)
 - [Notes to reviewer](#notes)
 - [App deployment steps](#deploy)


---

## App Architecture <a name="architecture" />
This infrastructure has the following modules:
 - VPC
 - Subnet
 - Bastion
 - Mongodb
 - app

High level overview:
The frontend app is behind a load balancer and connects to the MongoDB instance(s) via a dedicated internal load balancer for MongoDB hosts. There is one VPC for the project, and separate private and public subnets.

The Terraform modules are structured with the following files:
- `main.tf`: where the magic happens
- `input.tf`: input variables to the module
- `output.tf`: outputs from the module

There is a deployer key set as a placeholder in `modules/credentials` - this key is populated by Gitlab as part of the deploy process.

Additionally the remote state for this deployment is store in AWS, and the configuration can be seen in `environments/remote_state`.

### VPC
There is one VPC for the project and this module contains the main VPC resources, as well as the internet gateway.

### Subnet
Resources created here:
 - Private and public subnets
 - Base security group
 - Routing tables for subnets
 - NAT gateway and elastic IP for NAT


### Bastion
In order to connect to (for debugging, etc) the internal hosts, we'll want a bastion host set up for the relevant availability zones. This module creates a bastion host in the public subnet for access as a jumping point to internal machines.

### MongoDB
This is where the MongoDB instances are spun up. In this module, the load balancer for MongoDB instances, as well as the instance itself, are spun up. There is also a MongoDB specific security group created in this module.

The load balancer does a TCP health check against port `28017` (MongoDB's debug port) to make sure the host is healthy.

TODO: add in alerting/monitoring to when mongo hosts go down.

### App
This is a similar module to MongoDB in that it has a load balancer, the instance resource and a security group.

The load balancer does a TCP health check for port `8080` which is the port that the app serves it's HTML page on. It also forwards requests from port `80` on the internet at large to port `8080` for the app to serve up its static page.

---
## Potential next steps <a name="next-steps" />
While this is an example, next steps and potential edits to this structure would be:
 - Limit SSH connections to an internal corporate VPN (versus the internet at large, `0.0.0.0/0`)
 - There are some transient failures when starting up MongoDB, would want to implement a way to restart the service if/when it crashes

---
## Notes for reviewer <a name="notes" />
 - Using Gitlab for hosting this project to leverage CI/CD pipeline
 - Renamed `mongodb` to `mongo` for consistency in `test/main.tf`

---

## Deploy infrastructure <a name="deploy" />
To deploy infrastructure, first head over to [Pipelines](https://gitlab.com/stemarquardt/logikcull-take-home-assignment/pipelines), click "Run Pipeline", make sure `master` is selected then click "Create pipeline."

The pipeline needs manual approval to run, so wait until you see the gear / play icon over the "Start Deploy" stage. The Gitlab runners will handle the terraform deployment.

The output of `terraform apply` will give you the endpoints for the app test.htm and the mongoDB query endpoint. Once Terraform has completed running, it may take a couple of minutes for the app to become responsive (it's still registering with the load balancer), so hang tight for a few minutes before hitting the test HTML page.

### Deploy manually
In order to manually deploy this, you'll need to use your AWS credentials and apply the infrastructure in the following order:
 - `vpc`
 - `subnets`
 - `mongo`
 - Everything else

There are some dependencies between these that **should** be handled by passing a variable output from one into the other, but there were issues when getting the `length` of a list created by `vpc` and using it elsewhere, that it's better to explicitly apply the infrastructure in this order to ensure it comes out correct.
