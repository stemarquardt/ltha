# Create helpful outputs here..

output "VPC ID:" {
  value = "${module.vpc.vpc_id}"
}

output "VPC CIDR:" {
  value = "${module.vpc.vpc_cidr}"
}

output "App URL:" {
  value = "${module.app.app_lb}/test.htm"
}

output "Query URL:" {
  value = "${module.app.app_lb}/query.json"
}
