# NOTE: Setting a required version to help with compatibility
terraform {
  required_version = ">= 0.10.8"

  backend "s3" {
    bucket = "logikcull-assignment-state"
    key    = "tfstate"
    region = "us-east-1"
  }
}

# Configure the AWS Provider
provider "aws" {
  version = "~> 1.9"
  profile = "logikcull-test"
  region  = "us-east-1"
}

module "bastion" {
  source          = "../../modules/bastion"
  bastion_subnets = "${module.subnets.public_subnet_ids}"
  bastion_sg      = "${module.subnets.base_sg}"

  bastion_provisioning_key = "${var.provisioning_key}"
}

module "vpc" {
  source = "../../modules/vpc"

  vpc_name = "${var.vpc_name}" # Let's make sure this gets added as a tag
  vpc_cidr = "${var.vpc_cidr}"

  vpc_enable_dns_hostnames    = "${var.vpc_enable_dns_hostnames}"
  vpc_enable_dns_support      = "${var.vpc_enable_dns_support}"
  vpc_create_internet_gateway = "${var.vpc_create_internet_gateway}"

  vpc_tags = {
    Owner       = "${var.candidate_name}"
    Environment = "${var.environment}"
  }
}

module "subnets" {
  source = "../../modules/subnets"

  subnets_target_vpc_id   = "${module.vpc.vpc_id}"
  subnets_az_state_filter = "${var.subnets_az_state_filter}"
  subnets_private_count   = "${var.subnets_private_count}"   # Let's make sure to use distinct AZs for each
  subnets_public_count    = "${var.subnets_public_count}"
  subnets_cidr_block      = "${module.vpc.vpc_cidr}"
  subnets_igw_id          = "${module.vpc.vpc_igw_id}"

  subnets_enable_nat_gateway = "${var.subnets_enable_nat_gateway}"

  subnets_tags = {
    Owner       = "${var.candidate_name}"
    Environment = "${var.environment}"
  }
}

module "mongo" {
  source = "../../modules/mongodb"

  # What are we making
  mongo_count         = "${var.mongo_count}"
  mongo_ami           = "${var.mongo_ami}"
  mongo_instance_type = "${var.mongo_instance_type}"

  # Where to put it
  mongo_vpc_id           = "${module.vpc.vpc_id}"
  mongo_subnet_ids       = "${module.subnets.private_subnet_ids}"
  mongo_all_subnet_cidrs = "${module.subnets.all_subnet_cidr_blocks}"

  # How to build the disks and VM resources
  mongo_volume_type = "${var.mongo_volume_type}"
  mongo_volume_size = "${var.mongo_volume_size}"
  mongo_sg          = "${module.subnets.base_sg}"

  # How to provision it
  mongo_provisioning_key = "${var.provisioning_key}"
  mongo_bastion_hosts    = "${module.bastion.bastion_hosts}"

  # How to name and tag it
  mongo_tags = {
    Owner       = "${var.candidate_name}"
    Environment = "${var.environment}"
    Type        = "Database"
  }
}

module "app" {
  source = "../../modules/app"

  # What are we making
  app_count         = "${var.app_count}"
  app_ami           = "${var.app_ami}"
  app_instance_type = "${var.app_instance_type}"

  # Where to put it
  app_vpc_id  = "${module.vpc.vpc_id}"
  app_subnets = "${module.subnets.public_subnet_ids}"

  # How to provision it
  app_provisioning_key            = "${var.provisioning_key}"
  app_associate_public_ip_address = "${var.app_associate_public_ip_address}"
  app_sg                          = "${module.subnets.base_sg}"

  app_mongo_address = "${module.mongo.mongo_address}"

  # How to name and tag it
  app_tags = {
    Owner       = "${var.candidate_name}"
    Environment = "${var.environment}"
    Type        = "App"
  }
}
