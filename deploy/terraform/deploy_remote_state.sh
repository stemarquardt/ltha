DIR=$(dirname "$0")

cd $DIR/../../environments/remote_state

terraform init
terraform plan -out="remote_state.tfout" || exit $?
terraform apply -auto-approve remote_state.tfout
