DIR=$(dirname "$0")
MODULE="$1"

cd $DIR/../../environments/test

terraform init
terraform plan -out="${MODULE}.tfout" -target=module.${MODULE}
terraform apply -auto-approve "${MODULE}.tfout"
