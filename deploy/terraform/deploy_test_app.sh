DIR=$(dirname "$0")

cd $DIR/../../environments/test

terraform init
terraform plan -out=app.tfout || exit $?
terraform apply -auto-approve app.tfout
