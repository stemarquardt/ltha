output "mongo_address" {
  value = "${aws_elb.mongo_lb.dns_name}"
}
