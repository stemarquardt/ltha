# Put the cluster behind a load balancer so app can leverage one endpoint to get to mongodb
resource "aws_elb" "mongo_lb" {
  name     = "mongo-terraform-elb"
  internal = true

  listener {
    instance_port     = 27017
    instance_protocol = "tcp"
    lb_port           = 27017
    lb_protocol       = "tcp"
  }

  listener {
    instance_port     = 28017
    instance_protocol = "tcp"
    lb_port           = 28017
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:28017"
    interval            = 30
  }

  security_groups = ["${aws_security_group.mongo_sg.id}"]
  subnets         = ["${var.mongo_subnet_ids}"]
  instances       = ["${aws_instance.mongo.*.id}"]
}

resource "aws_instance" "mongo" {
  count = "${var.mongo_count}"

  ami           = "${var.mongo_ami}"
  instance_type = "${var.mongo_instance_type}"

  ebs_block_device {
    device_name = "/dev/sdb"
    volume_type = "${var.mongo_volume_type}"
    volume_size = "${var.mongo_volume_size}"
  }

  # Distribure instances across subnets
  subnet_id              = "${element(var.mongo_subnet_ids, count.index)}"
  vpc_security_group_ids = ["${var.mongo_sg}", "${aws_security_group.mongo_sg.id}"]

  # Execute startup script for mongo
  user_data = "${file("${path.module}/files/user_data.sh")}"

  key_name = "${var.mongo_provisioning_key}"

  tags = "${merge(var.mongo_tags, map("Name", "mongo-${count.index}"))}"
}

resource "aws_security_group" "mongo_sg" {
  name        = "allow_mongo"
  description = "Allow connections to mongo ports"

  vpc_id = "${var.mongo_vpc_id}"

  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["${var.mongo_all_subnet_cidrs}"]
  }

  ingress {
    from_port   = 28017
    to_port     = 28017
    protocol    = "tcp"
    cidr_blocks = ["${var.mongo_all_subnet_cidrs}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow_mongo"
  }
}
