variable "mongo_count" {
  description = "How many mongodb instances to create."
}

variable "mongo_ami" {
  description = "The ami to use for the mongodb instance. Here we're just using an Ubuntu 14.04 LTS public image."
}

variable "mongo_instance_type" {
  description = "The instance type to use for the mongodb instance."
}

variable "mongo_vpc_id" {
  description = "ID for mongodb VPC"
  type        = "string"
}

variable "mongo_subnet_ids" {
  description = "Private primary subnet ID for mongodb instance"
  type        = "list"
}

variable "mongo_all_subnet_cidrs" {
  description = "CIDR blocks for all subnets"
  type        = "list"
}

variable "mongo_sg" {
  description = "Security group for mongo instance"
}

variable "mongo_volume_type" {
  description = "The volume type to use for data storage on the mongodb instance."
}

variable "mongo_volume_size" {
  description = "The volume size to use for the mongodb instance."
}

variable "mongo_tags" {
  description = "Additional tags for the mongo setup"
  default     = {}
}

variable "mongo_provisioning_key" {
  description = "Provisioning key for VMs"
}

variable "mongo_bastion_hosts" {
  description = "Map of subnets to bastion public IP"
  type        = "list"
}
