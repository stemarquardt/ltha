output "public_subnet_ids" {
  value = "${aws_subnet.public_subnet.*.id}"
}

output "private_subnet_ids" {
  value = "${aws_subnet.private_subnet.*.id}"
}

output "all_subnet_cidr_blocks" {
  value = ["${aws_subnet.public_subnet.*.cidr_block}", "${aws_subnet.private_subnet.*.cidr_block}"]
}

output "base_sg" {
  value = "${aws_security_group.base_sg.id}"
}
