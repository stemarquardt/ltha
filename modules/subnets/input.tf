variable "subnets_target_vpc_id" {
  description = "VPC ID to place subnet in"
}

variable "subnets_az_state_filter" {
  description = "This value can be used to filter Availability Zones to use when setting up network resources. Let's only use available AZs as *data* sources"
}

variable "subnets_private_count" {
  description = "How many private subnets to make."
}

variable "subnets_public_count" {
  description = "How many public subnets to make. Just 1 is required for a NAT Gateway."
}

variable "subnets_cidr_block" {
  description = "CIDR block for subnets"
}

variable "subnets_enable_nat_gateway" {
  description = "Enable NAT gateway. Boolean"
}

variable "subnets_igw_id" {
  description = "Internet gateway ID for subnet."
  type        = "list"
}

variable "subnets_tags" {
  description = "Additional tags for the subnet"
  default     = {}
}
