data "aws_availability_zones" "available_azs" {
  state = "${var.subnets_az_state_filter}"
}

resource "aws_subnet" "private_subnet" {
  count  = "${var.subnets_private_count}"
  vpc_id = "${var.subnets_target_vpc_id}"

  # Choose a different availability zone for each subnet based on count index
  availability_zone = "${element(data.aws_availability_zones.available_azs.names, count.index)}"

  map_public_ip_on_launch = false
  cidr_block              = "${cidrsubnet(var.subnets_cidr_block, 8, count.index)}"

  tags = "${merge(var.subnets_tags, map("Name", format("Private Subnet")))}"
}

resource "aws_subnet" "public_subnet" {
  count             = "${var.subnets_public_count}"
  vpc_id            = "${var.subnets_target_vpc_id}"
  availability_zone = "${element(data.aws_availability_zones.available_azs.names, count.index)}"

  map_public_ip_on_launch = true

  # Want to make sure that our subnets don't collide - picking up at the cidr block where the
  # private subnet ends (subnets_private_count) and starting there for public cidr block.
  cidr_block = "${cidrsubnet(var.subnets_cidr_block, 8, var.subnets_private_count + count.index)}"

  tags = "${merge(var.subnets_tags, map("Name", format("Public Subnet")))}"
}

resource "aws_security_group" "base_sg" {
  name        = "allow_ssh"
  description = "Allow SSH connections"

  vpc_id = "${var.subnets_target_vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow_ssh"
  }
}

resource "aws_route_table" "public_route_table" {
  count  = "${length(var.subnets_igw_id)}"
  vpc_id = "${var.subnets_target_vpc_id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${element(var.subnets_igw_id, count.index)}"
  }

  tags {
    Name = "Public Subnet Routing Table"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = "${var.subnets_target_vpc_id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat_gateway.id}"
  }

  tags {
    Name = "Public Subnet Routing Table"
  }
}

resource "aws_route_table_association" "public_subnet_route_associations" {
  count          = "${length(var.subnets_igw_id)}"
  subnet_id      = "${element(aws_subnet.public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}

resource "aws_route_table_association" "private_subnet_route_associations" {
  count          = "${length(var.subnets_private_count)}"
  subnet_id      = "${element(aws_subnet.private_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_eip" "nat" {
  # Since we are importing internet gateway ID in this module, we don't need to explicitly
  # set a depends_on here since we know it'll be spun up first (since we're using the ID later)
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  # todo this doesnt work from fresh state
  count         = "${var.subnets_enable_nat_gateway ? var.subnets_public_count : 0}"
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${element(aws_subnet.public_subnet.*.id, count.index)}"
}
