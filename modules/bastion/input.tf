variable "bastion_ami" {
  description = "AMI for bastion host"
  default     = "ami-4d202037"
}

variable "bastion_subnets" {
  description = "List of subnets for bastion hosts"
  type        = "list"
}

variable "bastion_sg" {
  description = "Security group for bastion"
}

variable "bastion_provisioning_key" {
  description = "SSH key for provisioning bastion"
}
