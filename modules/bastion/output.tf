output "bastion_hosts" {
  # Map of bastion subnet to IP to use for looking up the right bastion host for a given subnet.
  value = "${aws_instance.bastion.*.public_ip}"
}
