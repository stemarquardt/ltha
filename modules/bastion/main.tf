resource "aws_instance" "bastion" {
  count = "${length(var.bastion_subnets)}"

  ami           = "${var.bastion_ami}"
  instance_type = "t2.micro"

  # Putting bastion host in each subnet, which themselves are in a
  # different availability zone
  subnet_id = "${element(var.bastion_subnets, count.index)}"

  associate_public_ip_address = true

  vpc_security_group_ids = ["${var.bastion_sg}"]

  key_name = "${var.bastion_provisioning_key}"

  tags = {
    Name = "Bastion-${count.index}"
    Type = "Bastion Host"
  }
}
