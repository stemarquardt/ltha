data "template_file" "user_data_script" {
  template = "${file("${path.module}/templates/user_data.tpl")}"

  vars {
    mongo_address = "${var.app_mongo_address}"
  }
}

# Put the cluster behind a load balancer so app can leverage one endpoint to get to mongodb
resource "aws_elb" "app_lb" {
  name     = "app-terraform-elb"
  internal = false

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:8080"
    interval            = 30
  }

  security_groups = ["${aws_security_group.app_sg.id}"]
  subnets         = ["${var.app_subnets}"]
  instances       = ["${aws_instance.app.*.id}"]
}

resource "aws_instance" "app" {
  count = "${var.app_count}"

  ami           = "${var.app_ami}"
  instance_type = "${var.app_instance_type}"

  associate_public_ip_address = "${var.app_associate_public_ip_address}"

  key_name = "${var.app_provisioning_key}"

  # Distribure instances across subnets
  subnet_id              = "${element(var.app_subnets, count.index)}"
  vpc_security_group_ids = ["${var.app_sg}", "${aws_security_group.app_sg.id}"]

  user_data = "${data.template_file.user_data_script.rendered}"

  tags = "${merge(var.app_tags, map("Name", "app-${count.index}"))}"
}

resource "aws_security_group" "app_sg" {
  name        = "allow_app"
  description = "Allow connections to 80 and 8080"

  vpc_id = "${var.app_vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow_app"
  }
}
