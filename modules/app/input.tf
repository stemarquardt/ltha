variable "app_count" {
  description = "Number of app front ends to spin up."
}

variable "app_ami" {
  description = "The ami to use for the application instance. Here we're just using an Ubuntu 14.04 LTS public image."
}

variable "app_instance_type" {
  description = "The instance type to use for the application instance."
}

variable "app_associate_public_ip_address" {
  description = "Associate a public ip address with an instance in a VPC. Boolean value."
}

variable "app_vpc_id" {
  description = "VPC ID for app to use"
  type        = "string"
}

variable "app_subnets" {
  description = "Subnet for the app"
  type        = "list"
}

variable "app_sg" {
  description = "Security group for app"
}

variable "app_provisioning_key" {
  description = "Provisioning key for app"
}

variable "app_mongo_address" {
  description = "Address to connect to mongodb instance"
}

variable "app_tags" {
  description = "Additional tags for the app"
  default     = {}
}
