#! /bin/bash
# This script validates the terraform for all directories in the input path parameter.
#
# ./validate.sh <path to terraform>
#
# Example usage:
# ./validate.sh environments

DIR=$(dirname "$0")
VALIDATE_DIR="$1"

cd $DIR/../../${VALIDATE_DIR}

for dir in $(ls .); do
    cd ${dir}
    echo "Validating Terraform in ${dir}"
    terraform init
    terraform validate || exit $?
    cd ../
done

exit 0
